%MATLAB R2020b
%name:airQuality
%author:maciej_pekala
%date: 17.12.2020
%version: v1.0
clear; clc;
% macierz dopuszczalnych norm
normy = [350 200 0 0 0 120 0 10000 0];
normy_24h= [125, 0, 0, 0, 0, 0, 0, 0, 50];

%plik tekstowy do zapisu wyników
wynik = fopen('wyniki.txt', 'w');

%otwarcie plików z danymi
data_1 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-02.csv");
data_2 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-03.csv");
data_3 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-04.csv");
data_4 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-05.csv");
data_5 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-06.csv");
data_6 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-07.csv");
data_7 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-08.csv");
data_8 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-09.csv");
data_9 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-10.csv");
data_10 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-11.csv");
data_11 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-12.csv");
data_12 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-13.csv");
data_13 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-14.csv");
data_14 = readtable("air_quality_data_Zabrze\dane-pomiarowe_2020-11-15.csv");




%odrzucenie wartości niepotrzebnych
data_1(25:27,:) = [];
data_2(25:27,:) = [];
data_3(25:27,:) = [];
data_4(25:27,:) = [];
data_5(25:27,:) = [];
data_6(25:27,:) = [];
data_7(25:27,:) = [];
data_8(25:27,:) = [];
data_9(25:27,:) = [];
data_10(25:27,:) = [];
data_11(25:27,:) = [];
data_12(25:27,:) = [];
data_13(25:27,:) = [];
data_14(25:27,:) = [];

%zmienna do nadpisywania
wszystkie_daty = ones(14*24,6);
wszystko= ones(14*24,9);


%zamiana przecinkow na kropki, zmiana typu danych, nadpisanie calosci do
%jednej zmiennej
all_data = {data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8, data_9, data_10, data_11, data_12, data_13, data_14};
all_dates = ["2020-11-02","2020-11-03","2020-11-04","2020-11-05","2020-11-06","2020-11-07", "2020-11-08","2020-11-09","2020-11-10","2020-11-11","2020-11-12","2020-11-13","2020-11-14","2020-11-15"];
for data_id = 1:14
    for godzina = 1:24
    temp = cell2mat(all_data{data_id}.SO2DwutlenekSiarkiPoz_Dop__350__g_m3_(godzina));
    temp = replace(temp, ",", ".");
    temp = string(temp);
    all_data{data_id}.SO2DwutlenekSiarkiPoz_Dop__350__g_m3_{godzina} = double(temp);
    
    temp2 = all_dates(data_id) + " " + num2str(godzina) + ":00";
    temp2 = datevec(temp2);
    
    wszystkie_daty((data_id-1)*24+godzina, :) = temp2;
    wszystko((data_id-1)*24+godzina, 1)= cell2mat(all_data{data_id}.SO2DwutlenekSiarkiPoz_Dop__350__g_m3_(godzina));
    wszystko((data_id-1)*24+godzina, 2)= all_data{data_id}.NO2DwutlenekAzotuPoz_Dop__200__g_m3_(godzina);
    wszystko((data_id-1)*24+godzina, 3)= all_data{data_id}.NOxTlenkiAzotu__g_m3_(godzina);
    wszystko((data_id-1)*24+godzina, 4)= all_data{data_id}.NOTlenekAzotu__g_m3_(godzina) ;
    wszystko((data_id-1)*24+godzina, 5)= all_data{data_id}.O3Ozon__g_m3_(godzina) ;
    wszystko((data_id-1)*24+godzina, 6)= all_data{data_id}.O3Ozon8hPoz_Doc__120__g_m3_(godzina) ;
    wszystko((data_id-1)*24+godzina, 7)= all_data{data_id}.COTlenekW_gla__g_m3_(godzina) ;
    wszystko((data_id-1)*24+godzina, 8)= all_data{data_id}.COTlenekW_gla8hPoz_Dop__10000__g_m3_(godzina) ;
    wszystko((data_id-1)*24+godzina, 9)= all_data{data_id}.PM10Py_ZawieszonyPM10__g_m3_(godzina) ;
    
    end
    
    
end

%porownanie z normami

for rzad = 1:336
    if wszystko(rzad, 1) > normy(1) &&  normy(1) > 0
        fprintf(wynik, "\n %d-%d-%d %d:00 SO2 zostało przekroczone %f", wszystkie_daty(rzad, 1:4), wszystko(rzad, 1));  
    end 
    
     if wszystko(rzad, 2) > normy(2) &&  normy(2) > 0
        fprintf(wynik, "\n %d-%d-%d %d:00 NO2 zostało przekroczone %f", wszystkie_daty(rzad, 1:4), wszystko(rzad, 2));  
     end 
    
    if wszystko(rzad, 3) > normy(3) &&  normy(3) > 0
        fprintf(wynik, "\n %d-%d-%d %d:00 NOx zostało przekroczone %f", wszystkie_daty(rzad, 1:4), wszystko(rzad, 3));  
    end 
    
     if wszystko(rzad, 4) > normy(4) &&  normy(4) > 0
        fprintf(wynik, "\n %d-%d-%d %d:00 NO zostało przekroczone %f", wszystkie_daty(rzad, 1:4), wszystko(rzad, 4));   
     end 
    
    if wszystko(rzad, 5) > normy(5) &&  normy(5) > 0
        fprintf(wynik, "\n %d-%d-%d %d:00 O3 zostało przekroczone %f", wszystkie_daty(rzad, 1:4), wszystko(rzad, 5));   
      end 
    
     if wszystko(rzad, 6) > normy(6) &&  normy(6) > 0
        fprintf(wynik, "\n %d-%d-%d %d:00 O3_8h zostało przekroczone %f", wszystkie_daty(rzad, 1:4), wszystko(rzad, 6));   
     end
     
    if wszystko(rzad, 7) > normy(7) &&  normy(7) > 0 
        fprintf(wynik, "\n %d-%d-%d %d:00 CO zostało przekroczone %f", wszystkie_daty(rzad, 1:4), wszystko(rzad, 7));   
    end
    
    if wszystko(rzad, 8) > normy(8) &&  normy(8) > 0
        fprintf(wynik, "\n %d-%d-%d %d:00 CO_8h zostało przekroczone %f", wszystkie_daty(rzad, 1:4), wszystko(rzad, 8));   
    end
    
    if wszystko(rzad, 9) > normy(9) &&  normy(9) > 0
        fprintf(wynik, "\n %d-%d-%d %d:00 PM10 zostało przekroczone %f", wszystkie_daty(rzad, 1:4), wszystko(rzad, 9));   
    end
    
   
   
end



%porownanie z normami 24h
for rzad = 1:336-23
if sum(wszystko(rzad:rzad+23, 1))/24 >  normy_24h(1) &&  normy_24h(1) > 0
fprintf(wynik, "\n %d-%d-%d %d:00 SO2 zostało przekroczone w ciagu 24h %f", wszystkie_daty(rzad+23, 1:4), sum(wszystko(rzad:rzad+23, 1)/24));      
end  

if sum(wszystko(rzad:rzad+23, 2))/24 >  normy_24h(2) &&  normy_24h(2) > 0
fprintf(wynik, "\n %d-%d-%d %d:00 NO2 zostało przekroczone w ciagu 24h %f", wszystkie_daty(rzad+23, 1:4), sum(wszystko(rzad:rzad+23, 2)/24));      
end 

if sum(wszystko(rzad:rzad+23, 3))/24 >  normy_24h(3) &&  normy_24h(3) > 0
fprintf(wynik, "\n %d-%d-%d %d:00 NOx zostało przekroczone w ciagu 24h %f", wszystkie_daty(rzad+23, 1:4), sum(wszystko(rzad:rzad+23, 3)/24));      
end 

if sum(wszystko(rzad:rzad+23, 4))/24 >  normy_24h(4) &&  normy_24h(4) > 0
fprintf(wynik, "\n %d-%d-%d %d:00 NO zostało przekroczone w ciagu 24h %f", wszystkie_daty(rzad+23, 1:4), sum(wszystko(rzad:rzad+23, 4)/24));      
end 

if sum(wszystko(rzad:rzad+23, 5))/24 >  normy_24h(5) &&  normy_24h(5) > 0
fprintf(wynik, "\n %d-%d-%d %d:00 O3 zostało przekroczone w ciagu 24h %f", wszystkie_daty(rzad+23, 1:4), sum(wszystko(rzad:rzad+23, 5)/24));      
end 



if sum(wszystko(rzad:rzad+23, 7))/24 >  normy_24h(7) &&  normy_24h(7) > 0
fprintf(wynik, "\n %d-%d-%d %d:00 CO zostało przekroczone w ciagu 24h %f", wszystkie_daty(rzad+23, 1:4), sum(wszystko(rzad:rzad+23, 7)/24));      
end 



if sum(wszystko(rzad:rzad+23, 9))/24 >  normy_24h(9) &&  normy_24h(9) > 0
fprintf(wynik, "\n %d-%d-%d %d:00 PM10 zostało przekroczone w ciagu 24h %f", wszystkie_daty(rzad+23, 1:4), sum(wszystko(rzad:rzad+23, 9)/24));      
end 
    
end    
    


%zliczenie ilosci przekroczenia norm
wynik_nazwa= 'wyniki.txt';
i= readlines(wynik_nazwa);

ilosc= length(i)-1%usuniecie pierwszego wolnego rzedu





